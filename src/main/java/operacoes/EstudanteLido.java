package operacoes;

public class EstudanteLido {
    public String nome;
    public int numero;
    public double nota;

    public EstudanteLido(String nome, int numero, double nota) {
        this.nome = nome;
        this.numero = numero;
        this.nota = nota;
    }
}
