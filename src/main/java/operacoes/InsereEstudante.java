package operacoes;

import dados.Estudantes;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class InsereEstudante {
//    Scanner sc = new Scanner(System.in);
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("Estudantes");
    EntityManager em = emf.createEntityManager();

    public List<EstudanteLido> leAlunos(){
        Scanner sc = new Scanner(System.in);
        System.out.println("quantos alunos vão ser inseridos no banco de dados? ");
        int n = sc.nextInt();
        List<EstudanteLido> estudantesLidos = new ArrayList<>();
        for (int i = 1; i <= n ; i++) {
             //dados dos estudantes
            System.out.println("Numero: ");
            int numero = sc.nextInt();
            System.out.println("Nome: ");
            String nome = sc.next();
            System.out.println("Nota: ");
            Double notas = sc.nextDouble();
            EstudanteLido estudantelido = new EstudanteLido(nome, numero, notas);
            estudantesLidos.add(estudantelido);
        }
        return estudantesLidos;
    }
    public void insereAlunos(List<EstudanteLido> estudantesLidos){
        for (EstudanteLido estudanteLido : estudantesLidos) {
            Estudantes estudante = new Estudantes();
            estudante.setNumero(estudanteLido.numero);
            estudante.setNome(estudanteLido.nome);
            estudante.setNota(estudanteLido.nota);

            em.getTransaction().begin();
            em.persist(estudante);
            em.getTransaction().commit();
        }
    }
    public static void main(String[] args){
        InsereEstudante insereEstudante = new InsereEstudante();
        List<EstudanteLido> estudantesLidos = insereEstudante.leAlunos();
        insereEstudante.insereAlunos(estudantesLidos);
    }
}
