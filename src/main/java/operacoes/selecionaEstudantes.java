package operacoes;

import dados.Estudantes;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class selecionaEstudantes {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Estudantes");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Estudantes f = em.find(Estudantes.class,2L);
        String nome = f.getNome();
        System.out.print(nome);


        em.getTransaction().commit();


    }
}
