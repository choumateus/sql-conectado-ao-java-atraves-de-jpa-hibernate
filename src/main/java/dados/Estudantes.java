package dados;

import javax.persistence.*;

@Entity
public class Estudantes {

    private Integer numero;
    private String nome;
    private Double nota;
    @Id
    @GeneratedValue
    private Long id;

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getNota() {
        return nota;
    }

    public void setNota(Double nota) {

        this.nota = nota;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}